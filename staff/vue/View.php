<?php

class View {
    // Variables de disposition:
    private $center, $centerLeft=null, $centerRight=null;
    
    // Variables interactions:
    private $nbEmprunt; //variable lignes emprunt
    private $nbRetour; //variable lignes retour
    private $tabResult;
    private $refAdherent;
    private $arrayDocRestant;
    private $dateReturn;
    
    
    // Constructeur:
    public function __construct() {
        // rien à faire
    }
    
    // Function Get attribut:
    public function __get($attr_name) {
        if (property_exists( __CLASS__, $attr_name)) { 
            return $this->$attr_name;
        } 
        $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
        throw new Exception($emess, 45);
    }

    // Function Set attribut:
    public function __set($attr_name, $attr_val) {
        if (property_exists( __CLASS__, $attr_name)) {
            $this->$attr_name=$attr_val; 
            return $this->$attr_name;
        } 
        $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
        throw new Exception($emess, 45);
    }    
    
    // Function affichage Debut Document Html:  
    private static function afficheDebutHtml(){
        $res= " <!DOCTYPE html>
                <html lang=\"fr\">
                    <head>
                        <meta charset=\"utf-8\">
                        <title> MediaNet </title>
                        <link rel=\"stylesheet\" href=\"stylesheets/reset_meyer.css\"> 
                        <link rel=\"stylesheet\" href=\"stylesheets/framework/framework.css\">
                        <link rel=\"stylesheet\" href=\"stylesheets/screen.css\">
                        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
                    </head>
                    <body>
                        <div>";
        return $res;
    }
    
    // Function affichage header:
    private function afficheHeader(){
        $header="   <header>
                        <div>
                            <a href='index.php'><img src='data/icons/home.png' alt='Accueil'></a>
                        </div>
                    </header>";
        return $header;
    }
    
    // Function affichage footer:
    private function afficheFooter(){
        $footer="   <footer>
                        <div>
                            <p>Copyright WADS - Medianet</p>
                        </div>
                    </footer>";
        return $footer;
    }     
    
    // Function affichage Fin Document Html:  
    private static function afficheFinHtml(){
        $res= "         </div>
                    </body>
                </html>";
        return $res;
    }
    
    // Function affichage page index:
    private static function afficheAccueil(){
        $res= "  <section id='index_staff'>
                    <div>
                        <article>
                            <h1><a href='index.php?action=action_emprunt'> EMPRUNT </a></h1>
                        </article>
                        
                        <article>
                            <h1><a href='index.php?action=action_retour'> RETOUR </a></h1>
                        </article>
                        
                        <article>
                            <h1><a href='index.php?action=action_search'> RECHERCHER </a></h1>
                        </article>
                        
                        <article>
                            <h1><a href='index.php?action=insert_user'> INSERER ADHERENT </a></h1>
                        </article>
                        <article>
                            <h1><a href='index.php?action=insert_sort'> INSERER GENRE </a></h1>
                        </article>
                    </div>
                </section>";
        return $res;
    }
    
    // Function affichage Form Search:
    private static function afficheFormSearch(){
        $res= " <article>
                    <h2> RECHERCHER </h2>
                    <form action='index.php?action=action_search' method='POST'>
                        <div>
                            <div>
                                <input name='keywords' type='text' placeholder='Mots-clés'>
                                <label><img src='./data/icons/search.png' alt='search'></label>
                            </div>
                            <div><select name='sort'><option value='Tous'> Tous Genre </option>";
                            //var_dump($_POST['s']);
                            foreach($_POST['s'] as $value){
                                $res .=" <option value='$value->label'> $value->label </option>";    
                            }

                                
                            $res .= "</select><label><img src='./data/icons/select.png' alt='select'></label>
                            </div>
                            <div>
                                <select name='type'>
                                    <option value='Tous'> Tout Type </option>
                                    <option value='Video'> Video </option>
                                    <option value='Audio'> Audio </option>
                                    <option value='Livre'> Livre </option>
                                </select>
                                <label><img src='./data/icons/select.png' alt='select'></label>
                            </div>
                        </div>
        		    <input type='hidden' value='action_search' name='action'>
                        <div>
                            <input type='reset' value=' '>
                            <input type='submit' value=' '>
                        </div>
                    </form>
                </article>";
        return $res;
    }
    
    /* Function affichage Results Search (liste résultat générée par fonction):
    private static function afficheResultSearch(){
        $res= " <article>".
                    // -->Need appel fonction:
                    "<div>
                        <h2> Titre </h2>
                        <h3> Type Genre </h3>
                        <h2> Author </h2>
                        <a href='index.php?action=action_document'> lien </a>
                        <img>
                    </div>
                </article>";
        return $res;
    }*/
    
    // Function affichage Results Search (liste rÃ©sultat gÃ©nÃ©rÃ©e par fonction):
    private static function afficheResultSearch($tabResult){ //$tabResult = $doc
        $res= " <article>";
        foreach ($tabResult as $value){
                $res .= "<div>";
                $res .= "<div>";
        	$res .= "<h2><a href='index.php?action=action_ficheDetail&amp;referenceDoc=".$value->reference."'>".$value->title."</a></h2>";
        	$res .= "<h3>".$value->author."</h3>";
        	$res .= "<h4>".$value->type." - ".$value->getSort()."</h4>";
        	$res .= "<p>".$value->statutDoc()."</p>";
                $res .= "</div>";
                $res .= "<div>";
                $res .= "<img src='data/images/image_not_found.png' alt='Image indisponible' >";
        	$res .= "</div>";
                $res .= "</div>";
        }
        $res .= "</article>";
        
        return $res;
    }
    
    // Function affichage page Search:
    private static function afficheSearch($centerLeft,$centerRight){
        $res= " <section id='search'>
                    <div>
                        $centerLeft
                        <hr>
                        $centerRight
                    </div>
                </section>";
        return $res;
    }
    
    /* Function affichage page Document:
    private static function afficheDocument(){
        $res= " <section>
                    <div>
                        <article>
                            <img src='defaut.png' alt='image'>
                            <div>
                                <h2> Titre </h2>
                                <p> Auteur </p>
                                <p> Editeur </p> <p> Type </p>
                                <h2> Disponible </h2>
                            </div>
                        </article>
                        
                        <article>
                            <p>
                                Description
                            </p>
                        </article>
                    </div>
                </section>";
        return $res;
    }*/
    
    //Function affichage page detailDoc:
    private static function afficheDetailDoc($tabResult, $dateReturn){
        $res = " <section id='detail'>";
        $res.= " <div>";
        
        $res .= "<article>";
        $res .=     "<div>";
        $res .=         "<img src='data/images/image_not_found.png' alt='Image indisponible' >";
        $res .=     "</div>";
        $res .=     "<div>";
        $res .=         "<h2>".$tabResult->title."</h2>";
        $res .=         "<h3>".$tabResult->author."</h3>";
        $res .=         "<h3>".$tabResult->dateParution."</h3>";
        //$res .= "<h3>".$value->dateAdd."</h3>";
        $res .=         "<h4>".$tabResult->type."</h4>";
        $res .=         "<h4>".$tabResult->getSort()."</h4>";
        $res .=         "<p>".$tabResult->statutDoc();
        // si le statut = emprunté alors on affiche la date de retour du document sinon rien
        if($tabResult->statutDoc()=="disponible"){
            $res .= "</p>";
            $res .= "<label id='dispo'></label>";
        }else{
            if($tabResult->statutDoc()=="emprunté"){
                $res .= " (retour : $dateReturn->dateReturn)</p>";
            }
            else{
                $res .= "</p>";
            }
            $res .= "<label id='indispo'></label>";
        }
        $res .=     "</div>";
        $res .= "</article>";
        
        $res .= "<article>";
        $res .=     "<p>".$tabResult->description."</p>";        
        $res .= "</article>";
        
        $res.= " </div>";
        $res.= " </section>";
        
	return $res;
    }
    
    // Function affichage page Emprunt:
    private static function afficheEmprunt($nbEmprunt){
        $res= " <section id='emprunt'>
                    <div>
                        <article>
                            <a href='index.php'></a>
                            <h2> NOUVEL EMPRUNT </h2>
                        </article>
                        
                        <form  action='index.php?action=action_recap_emprunt' method='post'>
                            <input type='text' placeholder='N° Adhérent' name='referenceAdherent'/>
                            <div  id='ajoutEmprunt'>
                                <input type='text' placeholder='Référence n°1' name='referenceDoc1'/>
                                <input type='text' placeholder='Référence n°2' name='referenceDoc2'/>
                                <input type='text' placeholder='Référence n°3' name='referenceDoc3'/>
                                
                            </div>
                            <script type=\"text/javaScript\">
                                var nb=4;
                                function ajouterEmprunt() {
                                    var contenu = document.createElement('input');
                                    contenu.type = \"text\";
                                    contenu.placeholder = \"Référence n°\"+nb+\"  \";
                                    contenu.name = \"referenceDoc\"+nb+\"\";
                                    document.getElementById('ajoutEmprunt').appendChild(contenu);
                                    nb++;
                                } 
                            </script>
                            <input type='button' onclick='ajouterEmprunt()' value=' '/>
                            <input type='submit' value='VALIDER EMPRUNT'>
                        </form>
                    </div>
                </section>";
        return $res;
    }
    
    // Function affichage page retour:
    private static function afficheRetour($nbRetour){
        $res= " <section id='retour'>
                    <div>
                        <article>
                            <a href='index.php'></a>
                            <h2> RETOUR EMPRUNT </h2>
                        </article>
                        
                        <form  action='index.php?action=action_recap_retour' method='post'>
                            <div  id='retourEmprunt'>
                                <input type='text' placeholder='Référence n°1' name='referenceDoc1'>
                                <input type='text' placeholder='Référence n°2' name='referenceDoc2'>
                                <input type='text' placeholder='Référence n°3' name='referenceDoc3'>                         
                            </div>
                            <script type=\"text/javaScript\">
                                var nb=4;
                                function retourEmprunt() {
                                    var contenu = document.createElement('input');
                                    contenu.type = \"text\";
                                    contenu.placeholder = \"Référence n°\"+nb+\"  \";
                                    contenu.name = \"referenceDoc\"+nb+\"\";
                                    document.getElementById('retourEmprunt').appendChild(contenu);
                                    nb++;
                                } 
                            </script>
                            <input type='button' value=' ' onclick='retourEmprunt()' />
                            <input type='submit' value='VALIDER RETOUR'>
                        </form>
                    </div>
                </section>";
        return $res;
    }
    
    // Function affichage page recap emprunt:
    private static function afficheRecapEmprunt($tabResult, $refAdherent){
        $nbEmprunt = count($tabResult);
        $res= " <section id='recap_emprunt'>
                    <div>
                        <article>
                            <h2> Récapitulatif d'emprunt </h2>
                            <p> N° Adhérent: $refAdherent </p>
                            <p> Nombre d'emprunt(s): $nbEmprunt </p>
                        </article>
                        <hr>";
        
                        if(!empty($tabResult)){
                            $res.="<div class='success'>Successful operation message</div>
                            <table>
                                <tr>
                                    <th> TITRE </th>
                                    <th> TYPE </th>
                                    <th> AUTHOR </th>
                                    <th> STATUT </th>
                                </tr>";
                            foreach ($tabResult as $value){
                                $res.="<tr>";
                                    $res.="<td>$value->title</td>";
                                    $res.="<td>$value->type</td>";
                                    $res.="<td>$value->author</td>";
                                    $res.="<td>".$value->statutDoc()."</td>";
                                    $res.="</tr>";
                            }
                            $res.="</table>";
                        }
                        
                       /* if (isset($_POST['error']) && !empty($_POST['error'])){
                            $res.="<div class='error'>Error message</div>
                            <table>
                                <tr>
                                    <th> TITRE </th>
                                    <th> TYPE </th>
                                    <th> AUTHOR </th>
                                    <th> STATUT </th>
                                </tr>";
                            foreach ($_POST['error'] as $value){
                                $res.="<tr>";
                                    $res.="<td>$value->title</td>";
                                    $res.="<td>$value->type</td>";
                                    $res.="<td>$value->author</td>";
                                    $res.="<td>".$value->statutDoc()."</td>";
                                    $res.="</tr>";
                            }
                            $res.="</table>";
                        }*/
                        
                        $res.="<article>
                            <a href='index.php'> Retour à l'accueil </a>
                        </article>
                    </div>
                </section>";
        return $res;
    }
    
    // Function affichage page recap retour:
    private static function afficheRecapRetour($tabResult, $refAdherent, $arrayDocRestant){
        $nbRetour = count($tabResult);
        $nbAgainLoaning = count($arrayDocRestant);
        $res= " <section id='recap_retour'>
                    <div>
                        <article>
                            <h2> Récapitulatif retour </h2>
                            <div>
                                <p> N° Adhérent: $refAdherent </p>
                            </div>
                            <div>
                                <p> Emprunt(s) retourné(s): $nbRetour </p>
                                <p> Emprunt(s) restant(s): $nbAgainLoaning </p>
                            </div>
                        </article>
                        <hr>
                        <article>
                            <h2> Retour(s): </h2>
                            <table>
                                <tr>
                                    <th> TITRE </th>
                                    <th> TYPE </th>
                                    <th> AUTHOR </th>
                                    <th> STATUT </th>
                                </tr>";
                                foreach ($tabResult as $value){
                                    $res.="<tr>";
                                    $res.="<td>$value->title</td>";
                                    $res.="<td>$value->type</td>";
                                    $res.="<td>$value->author</td>";
                                    $res.="<td>".$value->statutDoc()."</td>";
                                    $res.="</tr>";
                                }
                        
                            $res.="</table>
                        </article>
                        <hr>
                        <article>
                            <h2> Emprunt(s) restant(s): </h2>";
							if (count($arrayDocRestant)>0){//Si il n'y pas de document restant
								$res .="
								<table>
									<tr>
										<th> TITRE </th>
                                                                                <th> TYPE </th>
                                                                                <th> AUTHOR </th>
                                                                                <th> STATUT </th>
									</tr>";
									foreach ($arrayDocRestant as $value){
                                                                                $res.="<tr>";
										$res.="<td>$value->title</td>";
                                                                                $res.="<td>$value->type</td>";
                                                                                $res.="<td>$value->author</td>";
                                                                                $res.="<td>".$value->statutDoc()."</td>";
                                                                                $res.="</tr>";
									}
								
								$res.="</table>";
							}else{
								$res .="Aucun emprunt restant";
							}
                         $res.="</article>
                        
                        <article>
                            <a href='index.php'> Retour à l'accueil </a>
                        </article>
                    </div>
                </section>";
        return $res;
    }
	
	// Function affichage page des erreurs:
    private function afficheErreurs(){
		$res= "
        <div class='error'>";
		$res .= $_POST["erreurs"];	
		$res .="</div>";
		
		 return $res;
	}
    
    
    //function insert adherent
    private function formulaire_user(){
                $res = "
                        <section id='insert_adherent'>
                            <div>
                                <article>
                                    <h2> Ajouter un membre</h2>
                                </article>
                                <article>
                                    <form class='form-horizontal' action='index.php' method='POST'>
                                        <div>
                                            <h3>Infos Personnelles</h3>
                                            <input type='text' id='inputName' placeholder='Nom' name='name' >
                                            <input type='text' id='inputFirst' placeholder='Prénom' name='firstname' >
                                            <input type='text' id='inputDateAnn' placeholder='Naissance (aaaa-mm-jj)' name='birthday'>
                                            <input type='tel' id='inputTel' placeholder='Numéro de téléphone' name='numPhone'>
                                            <input type='number' placeholder='Numéro de rue' id='inputNum' name='numero' >
                                            <select name='type'>
                                                  <option value='rue'>Rue</option>
                                                  <option value='bis'>Bis</option>
                                                  <option value='ter'>Ter</option>
                                                  <option value='boulevard'>Boulevard</option>
                                            </select>
                                            <input  type='text' class='form-control' id='inputStreet' placeholder='Rue' name='street'>
                                            <input  type='text' class='form-control' id='inputCp' placeholder='CP' name='zip'>
                                            <input  type='text' class='form-control' id='inputCity' placeholder='Ville' name='city'>
                                        </div>
                                        <div>
                                            <h3>Compte</h3>
                                            <input type='text' id='inputLogin' placeholder='Identifiant' name='login' >
                                            <input type='password' id='inputPassword' placeholder='Mot de passe' name='password'>
                                            <input type='email' id='inputMail' placeholder='Mail' name='mail'>
                                            <input type='hidden'  id='form_action' value='insertUserOk' name='action' >
                                        </div>
                                        <input type='submit' value='Valider'>
                                    </form>
                                </article>
                            </div>
                        </section>
                ";
    	return $res;
    }
    
    
    private function formulaire_sort(){
        $res= " <section id='sort'>
                    <div>
                        <article>
                            <a href='index.php'></a>
                            <h2> NOUVEAU GENRE </h2>
                        </article>
                        
                        <form  action='index.php?action=index.php' method='post'>
                            <input type='text' placeholder='Nouveau genre' name='sort'>
                            <input type='hidden'  id='form_action' value='insertSortOk' name='action' >
                            <input type='submit' value='VALIDER GENRE'>
                        </form>
                    </div>
                </section>";
        return $res;
    }

    
    
    
    // Function affichage Général:
    public function afficheGeneral($select){
        switch($select){
            //Affichage de la page index:
            case 'index':
                $this->center=$this->afficheAccueil();
            break;
            //Afficher la page search:
            case 'search':
                $this->centerLeft=$this->afficheFormSearch();
                $this->centerRight=$this->afficheResultSearch($this->tabResult);
                $this->center=$this->afficheSearch($this->centerLeft,$this->centerRight);
            break;
            /*Afficher la page document:
            case 'document':
                $this->center=$this->afficheDocument();
            break;
            */
            case 'detailDoc':
                $this->center=$this->afficheDetailDoc($this->tabResult, $this->dateReturn);
            break;
            //Afficher la page emprunt:
            case 'emprunt':
                $this->center=$this->afficheEmprunt($this->nbEmprunt);
            break;
            //Afficher la page retour:
            case 'retour':
                $this->center=$this->afficheRetour($this->nbRetour);
            break;
            //Afficher la page recap emprunt:
            case 'recapEmprunt':
                $this->center=$this->afficheRecapEmprunt($this->tabResult, $this->refAdherent);
            break;
            //Afficher la page recap retour:
            case 'recapRetour':
                $this->center=$this->afficheRecapRetour($this->tabResult, $this->refAdherent, $this->arrayDocRestant);
            break;
            //affiche le formulaire insert d'un adherent
            case 'insert_user':
                $this->center=$this->formulaire_user();
            break;
            //affiche le formulaire insert d'un genre
            case 'insert_sort':
                $this->center=$this->formulaire_sort();
            break;
			//Afficher la page recap retour:
            case 'error':
                $this->center=$this->afficheErreurs();
            break;
        }
        echo $this->afficheDebutHtml();
        echo $this->afficheHeader();
        echo $this->center;
        echo $this->afficheFooter();
        echo $this->afficheFinHtml();
    }
    
}
?>