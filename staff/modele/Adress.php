<?php
 
 class Adress{
 	private $id;
 	private $num;
     private $type;
     private $street;
     private $zip;
     private $city;
     private $numAdherent;
 	
 	public function __construct() {
 	}
 	
 	public function __get($attr_name) {
 		if (property_exists( __CLASS__, $attr_name)) { 
 		  return $this->$attr_name;
 		} 
 		$emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
 		throw new Exception($emess, 45);
 	}
    
     public function __set($attr_name, $attr_val) {
 		if (property_exists( __CLASS__, $attr_name)) {
 		  $this->$attr_name=$attr_val; 
 		  return $this->$attr_name;
 		} 
 		$emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
 		throw new Exception($emess, 45);		
 	 }
     
     //********************************************************************************/
     //************************fonction insertAdresse*********************************/
     //*******************************************************************************/
     public function insert() {
         $db = Base::getConnection();
         $insert_query = $db->prepare("INSERT INTO adress VALUES (:id, :num, :type, :street, :zip, :city, :numAdherent)");
         Try{
             $insert_query->execute(array(":id"=>$this->id,":num"=>$this->num,":type"=>$this->type, ":street"=>$this->street, ":zip"=>$this->zip, ":city"=>$this->city, ":numAdherent"=>$this->numAdherent));
            $this->id = $db->lastInsertId('adress');
             //var_dump($a);
             //echo $db->lastInsertId('adherent');
         }catch(PDOExeption $e){
             new PDOExeption($e->getMessage());
         }
     }
 }
     
     
 ?>