<?php
 
 class Sort{
 	private $id;
 	private $label;
 	
 	public function __construct() {
 	}
 	
 	public function __get($attr_name) {
 		if (property_exists( __CLASS__, $attr_name)) { 
 		  return $this->$attr_name;
 		} 
 		$emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
 		throw new Exception($emess, 45);
 	}
    
     public function __set($attr_name, $attr_val) {
 		if (property_exists( __CLASS__, $attr_name)) {
 		  $this->$attr_name=$attr_val; 
 		  return $this->$attr_name;
 		} 
 		$emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
 		throw new Exception($emess, 45);		
 	 }
     
     //********************************************************************************/
     //************************fonction insertLabel*********************************/
     //*******************************************************************************/
     public function insert() {
         $db = Base::getConnection();
         $insert_query = $db->prepare("INSERT INTO sort VALUES (:id, :label)");
         Try{
             $insert_query->execute(array(":id"=>$this->id,":label"=>$this->label));
            $this->id = $db->lastInsertId('sort');
             //echo $db->lastInsertId('adherent');
         }catch(PDOExeption $e){
             new PDOExeption($e->getMessage());
         }
     }
 


//********************************************************************************/
//************************fonction findAll*********************************/
//*******************************************************************************/
    public static function findAll(){
		$db = Base::getConnection();
		$query = $db->prepare("SELECT * FROM sort");
		Try{
			$query->execute();
			$arraySortAll = Array();
			while ($d = $query->fetch(PDO::FETCH_OBJ)){ //fetchAll?
				$res = new Sort;
				$res->id = $d->id;
				$res->label = $d->label;
				$arraySortAll[] = $res;
			}
			return $arraySortAll;
		}catch(PDOExeption $e){
			new PDOExeption($e->getMessage());
		}
	}
     
 }
     
     
 ?>