<?php
class Document{

	private $reference ; 
	private $title;
	private $dateParution;
	private $author;
	private $description;
	private $dateAdd;
	private $idLibrary;
	private $idStatut;
	private $type;
	
	public function __construct(){
	
	}
	
	
	
	public function __toString() {
			return "[". __CLASS__ . "] reference : ". $this->reference.":
				title ". $this->title.":
				dateParution ". $this->dateParution.":
				author ". $this->author.":
				description ". $this->description.":
				dateAdd ". $this->dateAdd.":
				idLibrary ". $this->idLibrary.":
				idStatut ". $this->idStatut.":
				type ". $this->type;
	}




	public function __get($attr_name) {
		if (property_exists( __CLASS__, $attr_name)) { 
		  return $this->$attr_name;
		} 
		$emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
		throw new Exception($emess, 45);
	}
	   

	public function __set($attr_name, $attr_val) {
		if (property_exists( __CLASS__, $attr_name)) {
		  $this->$attr_name=$attr_val; 
		  return $this->$attr_name;
		} 
		$emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
		throw new Exception($emess, 45);	
	}
	
	public static function findBySort($sort){
		$db = Base::getConnection();
		$query = $db->prepare("SELECT * FROM document JOIN document_sort WHERE document.reference = document_sort.referenceDoc AND idSort=(SELECT id FROM sort WHERE label=:sort)"); //SELECT * FROM document, document_sort NATURAL JOIN sort WHERE sort=:cat_id
		Try{
			$query->execute(array(":sort"=>$sort));
			$arrayDocSort = Array();
			while ($d = $query->fetch(PDO::FETCH_OBJ)){ /*fetchAll?*/
				$res = new Document;
				$res->reference = $d->reference;
				$res->title = $d->title;
				$res->dateParution = $d->dateParution;
				$res->author = $d->author;
				$res->description = $d->description;
				$res->dateAdd = $d->dateAdd;
				$res->idLibrary = $d->idLibrary;
				$res->idStatut = $d->idStatut;
				$res->type = $d->type;
		
				$arrayDocSort[] = $res;
		
			}
			return $arrayDocSort;
		}catch(PDOExeption $e){
			new PDOExeption($e->getMessage());
		}
	}
	
	public static function findAll(){
		$db = Base::getConnection();
		$query = $db->prepare("SELECT * FROM document");
		Try{
			$query->execute();
			$arrayDocAll = Array();
			while ($d = $query->fetch(PDO::FETCH_OBJ)){ //fetchAll?
				$res = new Document;
				$res->reference = $d->reference;
				$res->title = $d->title;
				$res->dateParution = $d->dateParution;
				$res->author = $d->author;
				$res->description = $d->description;
				$res->dateAdd = $d->dateAdd;
				$res->idLibrary = $d->idLibrary;
				$res->idStatut = $d->idStatut;
				$res->type = $d->type;
				$arrayDocAll[] = $res;
			}
			return $arrayDocAll;
		}catch(PDOExeption $e){
			new PDOExeption($e->getMessage());
		}
	}
	
   /* public static function findByReference($reference){
    	$db = Base::getConnection();
    	$query = $db->prepare("SELECT * FROM document WHERE reference=:reference");
    	Try{
    		$query->execute(array(":reference"=>$reference));
    		$arrayDocByRef = Array();
    		while ($d = $query->fetch(PDO::FETCH_OBJ)){ //fetchAll?
    			$res = new Document;
    			$res->reference = $d->reference;
    			$res->title = $d->title;
    			$res->dateParution = $d->dateParution;
    			$res->author = $d->author;
    			$res->description = $d->description;
    			$res->dateAdd = $d->dateAdd;
    			$res->idLibrary = $d->idLibrary;
    			$res->idStatut = $d->idStatut;
    			$res->type = $d->type;
    	
    			$arrayDocByRef[] = $res;
    		}
    		return $arrayDocByRef;
    	}catch(PDOExeption $e){
    		new PDOExeption($e->getMessage());
    	}
    }*/
	
	 public static function findByReference($reference){
    	$db = Base::getConnection();
    	$query = $db->prepare("SELECT * FROM document WHERE reference=:reference");
    	Try{
    		$query->execute(array(":reference"=>$reference));
    		$arrayDocByRef = Array();
    		$d = $query->fetch(PDO::FETCH_OBJ);
			if($d != false){
    			$res = new Document;
    			$res->reference = $d->reference;
    			$res->title = $d->title;
    			$res->dateParution = $d->dateParution;
    			$res->author = $d->author;
    			$res->description = $d->description;
    			$res->dateAdd = $d->dateAdd;
    			$res->idLibrary = $d->idLibrary;
    			$res->idStatut = $d->idStatut;
    			$res->type = $d->type;
			}else{
				$res = null;
			}
    	
    	
    		return $res;
    	}catch(PDOExeption $e){
    		new PDOExeption($e->getMessage());
    	}
    }
    
    public static function findByType($type){
    	$db = Base::getConnection();
    	$query = $db->prepare("SELECT * FROM document WHERE type=:type");
    	Try{
    		$query->execute(array(":type"=>$type));
    		$arrayDocByType = Array();
    		while ($d = $query->fetch(PDO::FETCH_OBJ)){ //fetchAll?
    			$res = new Document;
    			$res->reference = $d->reference;
    			$res->title = $d->title;
    			$res->dateParution = $d->dateParution;
    			$res->author = $d->author;
    			$res->description = $d->description;
    			$res->dateAdd = $d->dateAdd;
    			$res->idLibrary = $d->idLibrary;
    			$res->idStatut = $d->idStatut;
    			$res->type = $d->type;
    			 
    			$arrayDocByType[] = $res;
    		}
    		return $arrayDocByType;
    	}catch(PDOExeption $e){
    		new PDOExeption($e->getMessage());
    	}
    }
    
    public static function findByKeywords($keywords){
    	$db = Base::getConnection();
    	$query = $db->prepare("SELECT * FROM document WHERE document.title LIKE ? OR document.description LIKE ?");
    	Try{
    		$query->execute(array("%$keywords%", "%$keywords%"));
    		$arrayDocByType = Array();
    		while ($d = $query->fetch(PDO::FETCH_OBJ)){ //fetchAll?
    			$res = new Document;
    			$res->reference = $d->reference;
    			$res->title = $d->title;
    			$res->dateParution = $d->dateParution;
    			$res->author = $d->author;
    			$res->description = $d->description;
    			$res->dateAdd = $d->dateAdd;
    			$res->idLibrary = $d->idLibrary;
    			$res->idStatut = $d->idStatut;
    			$res->type = $d->type;
    
    			$arrayDocByType[] = $res;
    		}
    		return $arrayDocByType;
    	}catch(PDOExeption $e){
    		new PDOExeption($e->getMessage());
    	}
    }
	
	public function statutDoc(){ //document->idStatut
		$db = Base::getConnection();
		$query = $db->prepare("SELECT label FROM statut WHERE id=:idStatut");
		Try{
    		$query->execute(array(":idStatut"=>($this->idStatut)));
			$res = $query->fetch();
			return $res['label'];
    	}catch(PDOExeption $e){
    		new PDOExeption($e->getMessage());
    	}
	}
	
	public function getSort(){ //On sort de l'activ record...
		$db = Base::getConnection();
		$query = $db->prepare("SELECT label FROM sort JOIN document_sort JOIN document WHERE document_sort.idSort = sort.id AND document.reference = document_sort.referenceDoc AND document.reference=:reference");
		Try{
    		$query->execute(array(":reference"=>($this->reference)));
			$res = $query->fetch();
			return $res['label'];
    	}catch(PDOExeption $e){
    		new PDOExeption($e->getMessage());
    	}
	}
    
    
    public function update(){
        if (!isset($this->reference)) {
            throw new Exception(__CLASS__ . ": Primary Key undefined : cannot update");
        } 


        $save_query = "UPDATE document SET title=".(isset($this->title) ? "'$this->title'" : "null").",
                            dateParution=".(isset($this->dateParution) ? "'$this->dateParution'" : "null").",
                            author=".(isset($this->author) ? "'$this->author'" : "null").",
                            description=".(isset($this->description) ? "'$this->description'" : "null").",
                            idStatut=".(isset($this->idStatut) ? "'$this->idStatut'" : "null").",
                            type=".(isset($this->type) ? "'$this->type'" : "null")
                            ." where reference=$this->reference";
        $db = Base::getConnection();
        $nb=$db->exec($save_query);

        return $nb;
	
    }
    
    public function insert() {
        $db = Base::getConnection();
        $insert_query = $db->prepare("INSERT INTO document VALUES (:reference, :title, :dateParution, :author, :description, CURRENT_TIMESTAMP, :idLibrary, :idStatut, :type)");
        Try{
            $insert_query->execute(array(":reference"=>$this->reference, ":title"=>$this->title,":dateParution"=>$this->dateParution, ":author"=>$this->author, ":description"=>$this->description, ":idLibrary"=>$this->idLibrary, ":idStatut"=>$this->idStatut, ":type"=>$this->type));
           $this->reference = $db->lastInsertId('document');
            /*echo $db->lastInsertId('document');*/
        }catch(PDOExeption $e){
            new PDOExeption($e->getMessage());
        }
    }
}
?>
 
