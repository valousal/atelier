<?php
	class Base{
		private static $db;
		
		/*établie une connexion a la base et retourne une instance de PDO*/
		private static function connect(){
			Try{
				$config = parse_ini_file("config.ini");
				$user = $config['user'];
				$pass = $config['password'];
				$dbname = $config['dbname'];
				$hostname = $config['hostname'];
				// $dsn="mysql:host='$hostname';dbname='$dbname'";
				$dsn="mysql:host=$hostname;dbname=$dbname";
				$db = new PDO($dsn, $user,$pass, array(PDO::ERRMODE_EXCEPTION=>true, PDO::ATTR_PERSISTENT=>true));
			}catch(PDOException $e){ 
				throw new PDOException("connection error: $dsn".$e->getMessage(). '<br/>');
			}
			$db->exec("SET CHARACTER SET utf8");
			return $db;
		}
		
		/*vérifie si un lien existe le crée le cas échéant et le retourne*/
		public static function getConnection(){
			if (isset(self::$db)){
				return self::$db;
			}else {
				self::$db = self::connect();
				return self::$db;
			}
		}
	}
?>