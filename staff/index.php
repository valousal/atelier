<?php

    // Function Autoload des classes:
    function __autoload($class_name) {
        //Tableau contenant la liste des dossiers contenants des classes a importer:
        $array_directory = array(
            'controleur/',
            'vue/',
            'modele/'
        );
        //On parcourt l'ensemble des dossiers:
        foreach($array_directory as $directory)
        {
            //On regarde si la classe existe dans le dossier:
            if(file_exists($directory.$class_name . '.php'))
            {
                require_once($directory.$class_name . '.php');
            }            
        }
    }
    
    // On cr�� un nouveau controleur:
    $c=new Controleur();
    
    if(isset($_REQUEST['action']) && $_REQUEST !=NULL){
	$_REQUEST=$_REQUEST;
    }
    else{
	$_REQUEST['action']='action_index';
    }
    
    echo $c->dispatch($_REQUEST);

?>