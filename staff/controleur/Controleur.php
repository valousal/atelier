<?php

class Controleur {

    // Function Controleur:
    public function __construct() {
        // rien à faire
    }

    // Function Dispatch:
    public function dispatch($get){
        switch($get['action']){
            case 'action_index':
                $this->actionPageIndex();
            break;
            case 'action_search':
                $this->actionPageSearch();
            break;
            /*case 'action_document':
                $this->actionPageDocument();
            break;*/
            case 'action_ficheDetail':
                $this->actionFicheDetail();
            break;
            case 'action_emprunt':
                $this->actionPageEmprunt($get); // Nécessite $nbEmprunt
            break;
            case 'action_retour':
                $this->actionPageRetour($get); // Nécessite $nbRetour
            break;
            case 'action_recap_emprunt':
                $this->actionPageRecapEmprunt($get); // Nécessite $nbRetour
            break;
            case 'action_recap_retour':
                $this->actionPageRecapRetour($get); // Nécessite $nbRetour
            break;
            case 'insert_user': //insert_user
				$this->insertUser();
            break;
            case 'insertUserOk' : //insert user validé
                $this->insertUserOk();
            break;
            case 'insert_sort': //formulaire insert sort
				$this->insertSort();
            break;
            case 'insertSortOk' : //insert sort validé
                $this->insertSortOk();
            break;
			case 'action_error':
                $this->actionPageError($get); // Nécessite $nbRetour
            break;
        }
    }
    
    //  Action pour afficher page index:
    private function actionPageIndex(){
        $v=new View();
        $v->afficheGeneral('index');
    }
    
    //  Action pour afficher page search:
    private function actionPageSearch(){
        $v=new View();
        $d = new Document();
        
        $s = Sort::findAll();
        $_POST['s'] = $s;
        
        //Trier en fonction du type
        if (isset($_POST['type'])){
        	$type = $_POST['type'];
        	if ($type == 'Tous'){
        		$doctype = $d->findAll();
        	}else{
        		$doctype = $d->findByType($type);
        	}
        }else{
        	$doctype = $d->findAll();
        }
       
        //Trier en fonction du genre
        if (isset($_POST['sort'])){
        	$sort = $_POST['sort'];
        	if ($sort == 'Tous'){
        		$docSort = $d->findAll();
        	}else{
				$docSort = $d->findBySort($sort);
			}        
        }else{
        	$docSort = $d->findAll();
        }
        
        //Trier en fonction du mot cle
        if (isset($_POST['keywords'])){
        	$keywords = $_POST['keywords'];
        	if (empty($keywords)){
        		$docKey = $d->findAll();
        	}else{
        		$docKey = $d->findByKeywords($keywords);
        	}
        }else{
        	$docKey = $d->findAll();
        }

        $v->tabResult = array_intersect($doctype, $docSort, $docKey); //array_merge($doctype,$doc)
        $v->afficheGeneral('search');
    }
    
    /*  Action pour afficher page d'un document:
    private function actionPageDocument(){
        $v=new View();
        $v->afficheGeneral('document');
    }*/
    
    //Action pour afficher détaile d'un Document
    private function actionFicheDetail(){
           $v=new View();
           //$d = new Document();
           
           //Récupe infos Document par Reference
           $reference = $_GET['referenceDoc'];
           $document = Document::findByReference($reference);
           //var_dump($document); 
    
            //verifie si l'id statut est a 2 soit si le document est emprunté afin d'afficher la date de retour 
            foreach($document as $value){
                if($value->idStatut == 2){ 
                    $documentLoaning = Loaning::findEmpruntByReferenceDoc($reference);
                    $v->dateReturn = $documentLoaning;
                }
            }
    
           $v->tabResult = $document; 
           $v->afficheGeneral('detailDoc');
    }
    
    //  Action pour afficher page emprunt:
    private function actionPageEmprunt($get){
        $v=new View();
        if(isset($get['nbEmprunt'])){
            $v->nbEmprunt=$get['nbEmprunt'];
        }else{
            $v->nbEmprunt=3;
        }
        $v->afficheGeneral('emprunt');
    }
    
    //  Action pour afficher page retour:
    private function actionPageRetour($get){
        $v=new View();
        if(isset($get['nbRetour'])){
            $v->nbRetour=$get['nbRetour'];
        }else{
            $v->nbRetour=3;
        }
        $v->afficheGeneral('retour');
    }
    
    //  Action pour afficher page RecapEmprunt:
    private function actionPageRecapEmprunt($get){
        $v=new View();
        $l = new Loaning();
        $tempEmprunt = time() + 1728000;// ===== 20 jours
        
		$arrayLoaning = array();
		
        //recup les info du document en fonction de la reference
        $referenceAdherent = $_POST['referenceAdherent'];
        $a = Adherent::findById($referenceAdherent);
        if($a != null)
        {
            $i = 1; //iteration pour recup tous les input du formulaire ajout emprunt
            $arrayDoc= array();
           // $arrayDocError = array();
			
			
			//Test si la reference de Document rentrée existe
			while (isset($_POST["referenceDoc".$i.""]) && !empty($_POST["referenceDoc".$i.""])){
                $referenceDoc = $_POST["referenceDoc".$i.""];
                $doc = Document::findByReference($referenceDoc);
				if ($doc == null){
					$_POST["erreurs"] = "Impossible de faire un emprunt, le document <b>".$referenceDoc."</b> n'est pas répertorié";
					$v->afficheGeneral('error');
					exit();
				}else{
					if($doc->idStatut == 2){
						//si deja emprunte
						$_POST["erreurs"] = "Le document <b>".$referenceDoc."</b> n'est actuellement pas disponible";
						$v->afficheGeneral('error');
						exit();
					}else{
						/*$doc->idStatut= 2;
						$doc->update();*/
						$arrayDoc[$i] = $doc;
						
						/*************\
						Function pour créer les emprunt dans la table Loaning!!!
						\*************/
						$l1= new Loaning();
						
						$l1->numAdherent = $referenceAdherent;
						$l1->referenceDoc = $referenceDoc;
						$l1->dateReturn =  date('Y-m-d h:i:s', strtotime('+20 days')); 
						$arrayLoaning[] = $l1;
						/*$l1->insert();*/
					}
				}
				$i++;
			}
			
			foreach ($arrayDoc as $value){
				$value->idStatut= 2;
				$value->update();
			}
			
			foreach ($arrayLoaning as $value){
				$value->insert();
			}
			
			
            $v->tabResult = $arrayDoc;
            $v->refAdherent = $referenceAdherent;
            $v->afficheGeneral('recapEmprunt');
        }else{
			$_POST["erreurs"] = "Le numéro d'adhérent <b>".$referenceAdherent."</b> n'existe pas";
			$v->afficheGeneral('error');
        }
    }
    
    //  Action pour afficher page RecapRetour:
    private function actionPageRecapRetour($get){
        $v=new View();
        
        $l = new Loaning();		
		$i = 1; //iteration pour recup tous les input du formulaire ajout emprunt
		$arrayDoc= array();
		$arrayRef=array();
		
		/************\
          Test si les documents retournées ont bien été emprunté au préalable
        /************/
		while (isset($_POST["referenceDoc".$i.""]) && !empty($_POST["referenceDoc".$i.""])){
			$referenceDoc = $_POST["referenceDoc".$i.""];
            $doc = Document::findByReference($referenceDoc);
			if ($doc == null){
				$_POST["erreurs"] = "Impossible de faire un retour, le document <b>".$referenceDoc."</b> n'est pas emprunté";
				$v->afficheGeneral('error');
				exit();
			}else{
				$arrayDoc[$i] = $doc;
				/************\
				function pour recupere num adherent
				/************/
				$findref = Loaning::findEmpruntByReferenceDoc($referenceDoc);
				$refAdherent = $findref->numAdherent;
				
				 /************\
				function pour changer idStatut
				/************/
				/*$doc->idStatut= 1;
				$doc->update();*/
				
				
				/*************\
				Function pour supprimer les emprunt dans la table Loaning!!!
				\*************/
				/*$l->referenceDoc = $referenceDoc;*/
				$arrayRef[$i]=$referenceDoc;
				/*var_dump($arrayLoading);*/
				/*$l->delete();*/
				$i++;
			}
		}
		
		 /************\
		function pour changer idStatut (après la boucle)
		/************/
		foreach ($arrayDoc as $value){
			$value->idStatut= 1;
			$value->update();
		}
		
		/*************\
		Function pour supprimer les emprunt dans la table Loaning! en dehor de la boucle pour exécuter que si'il n'y a pas d'erreur
		\*************/
		foreach ($arrayRef as $value){
			$l->referenceDoc = $value;
			$l->delete();
        }
        /*************\
        Function pour recuperer les doc encore emprunté par l'adherent
        \*************/
        $againLoaning = Loaning::findEmpruntByNumAdherent($refAdherent);
        
		$arrayDocRestant = array();
		foreach ($againLoaning as $value){
			$d = Document::findByReference($value->referenceDoc);
			$arrayDocRestant[] = $d;
		}
			
		//var_dump($againLoaning);
		$v->arrayDocRestant = $arrayDocRestant;
		$v->tabResult = $arrayDoc;
		$v->refAdherent = $refAdherent;
		$v->afficheGeneral('recapRetour');
    }
    
    
    
    
    private function insertUser(){
		$v = new View();
		$v->afficheGeneral('insert_user');
	}
	
	
	private function insertUserOk(){
		// ajout au moment du clic !!
		$v = new View();
        
        $ad = new Adherent();
        $ad->name = $_POST['name'];
        $ad->firstname = $_POST['firstname'];
        $ad->mail = $_POST['mail'];
        //$ad->dateRegistration = $_POST['date'];
        $ad->birthday = $_POST['birthday'];
        $ad->numPhone = $_POST['numPhone'];
        $ad->login = $_POST['login'];
        $h = 
        $salt = time(); //generefer chaine de caract aleatoire
        $ad->password = crypt($_POST['password'], $salt);
        $ad->salt = $salt;
        $ad->idLibrary = 1;
        
        $ad->insert();
		
        //ajout de l'adresse de l'adherent
        $adress = new Adress();
		$adress->num = $_POST['numero'];
       $adress->type = $_POST['type'];
       $adress->street = $_POST['street'];
       $adress->zip = $_POST['zip'];
       $adress->city = $_POST['city'];
       $adress->numAdherent = $ad->numAdherent;
       $adress->insert();
        
		//
		//$b_insert->salt = $salt;
        
        
        //renvoyer le numero de reference de l'adherent après l'insertion !!
		$v->afficheGeneral('index');
	}
    

    

	 
    //  Action pour afficher la page d'erreur:
    private function actionPageError(){
		$v=new View();
		$v->afficheGeneral('error');	
	}
    
    
    // action affiche le formulaire insert sort
    private function insertSort(){
		$v = new View();
		$v->afficheGeneral('insert_sort');
	}
    
    // action validation insert sort
    private function insertSortOk(){
		$v = new View();
        
        $ad = new Sort();
        $ad->label = $_POST['sort'];
        if(!empty($ad->label) && isset($ad->label)){
            $ad->insert();
        }else{
            $_POST["erreurs"] = "Impossible d'ajouter un genre";
            $v->afficheGeneral('error');
            exit();
        }
		$v->afficheGeneral('index');
	}
	
	
}
?>   