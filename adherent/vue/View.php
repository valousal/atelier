<?php

class View {
    private $center, $centerLeft=null, $centerRight=null, $tabResult, $dateReturn;
    
    // Constructeur:
    public function __construct() {
        // rien Ã  faire
    }
    
    // Function Get attribut:
    public function __get($attr_name) {
        if (property_exists( __CLASS__, $attr_name)) { 
            return $this->$attr_name;
        } 
        $emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
        throw new Exception($emess, 45);
    }

    // Function Set attribut:
    public function __set($attr_name, $attr_val) {
        if (property_exists( __CLASS__, $attr_name)) {
            $this->$attr_name=$attr_val; 
            return $this->$attr_name;
        } 
        $emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
        throw new Exception($emess, 45);
    }    
    
    // Function affichage Debut Document Html:  
    private static function afficheDebutHtml(){
        $res= " <!DOCTYPE html>
                <html lang=\"fr\">
                    <head>
                        <meta charset=\"utf-8\">
                        <title> MediaNet </title>
                        <link rel=\"stylesheet\" href=\"stylesheets/reset_meyer.css\"> 
                        <link rel=\"stylesheet\" href=\"stylesheets/framework/framework.css\">
                        <link rel=\"stylesheet\" href=\"stylesheets/screen.css\">
                        <meta name='viewport' content='width=device-width, initial-scale=1.0'>
                    </head>
                    <body>
                        <div>";
        return $res;
    }
    
    // Function affichage header:
    private function afficheHeader(){
        $header="   <header>
                        <div>";
                            $s = new Session();
			    if(!$s->is_logged_in()) {
                            
                            $header .="<form action='index.php' method='post'>
                                    <input type='text' name='login' id='login' placeholder='Identifiant'>
                                    <input type='password' name='password' id='password' placeholder='Mot de passe'>
                                    <input type='submit' value='Connexion'>
                                    <input type='hidden' value='authOk' name='action' >
                                    <a href='index.php'><img src='data/icons/home.png' alt='Accueil'></a>
                            </form>";
                           }else{
			    $header .=" <div><form action='index.php' method='post'>
                                            <input type='submit' value='Mon Profil'>
                                            <input type='hidden' value='".$s->user_id."' name='nomAdherent' >
                                            <input type='hidden' value='action_profil' name='action' >
                                        </form>";
							  //$header .= "<a href='index.php?action=action_profil&ampnomAdherent=".$s->user_id."'>Profil</a>";
                            $header .=" <form action='index.php?action=logout' method='post'>
                                            <input type='submit' value='Deconnexion'>
                                            <a href='index.php'><img src='data/icons/home.png' alt='Accueil'></a>
                                        </form></div>";
			}
                        
                    $header .= "</div>
                    </header>";
        return $header;
    }
    
    // Function affichage footer:
    private function afficheFooter(){
        $footer="   <footer>
                        <div>
                            <p>Copyright WADS - Medianet</p>
                        </div>
                    </footer>";
        return $footer;
    }   
    
    // Function affichage Fin Document Html:  
    private static function afficheFinHtml(){
        $res= "         </div>
                    </body>
                </html>";
        return $res;
    }
    
    // Function affichage page index:
    private static function afficheAccueil(){ //class='btn btn-lg'
        $res= " <section id='jumbotron'>
                    <div>
                        <h2> Medianet </h2>
                        <a href='index.php?action=action_search'> RECHERCHER </a> 
                    </div>
                </section>
                
                <section id='index_adherent'>
                    <div>
                        <article>
                            <h2> Nouveautés </h2>
							<p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. In quis nisi egestas, ultricies nisi ac, interdum tellus. Proin egestas congue urna consectetur vestibulum. Vivamus aliquam urna ut libero gravida, quis laoreet urna dictum. Sed sagittis facilisis tellus, posuere bibendum enim faucibus nec. Nullam vel gravida purus. Nulla id risus lorem. In hac habitasse platea dictumst.

Duis iaculis, nisi eu commodo elementum, lorem tellus malesuada risus, nec blandit diam erat non augue. Praesent sed malesuada tellus, in finibus mauris. Cras eu lacus eget libero ultricies efficitur. Duis fringilla tincidunt eros ut fermentum. Praesent rhoncus vel diam sit amet eleifend. Nam nec imperdiet nibh, nec sodales dui. Duis suscipit id eros nec posuere. Donec viverra tellus eget metus porttitor tempor. Mauris ut sapien luctus, porttitor eros a, suscipit libero. Nunc iaculis augue vitae nulla dignissim, vitae blandit nisi malesuada. Etiam turpis erat, volutpat eu velit at, euismod venenatis velit. Etiam et elit ut ipsum pellentesque mollis.</p>
                        </article>
                        
                        <article>
                            <h2> Description </h2>
                            <p>Lieu d'animation, d'information et de loisirs, la Médiathèque de l'Orangerie, équipement communautaire géré par la                                   Communauté de Communes du Lunévillois, propose un vaste choix de documents, toujours réactualisés. Inaugurée le 16 mai                                  1995, elle comprend des espaces adulte, audiovisuel, multimédia et un secteur jeunesse dans lesquels vous trouverez                                     livres, périodiques, CD, CD ROM, DVD, partitions, cassettes, tant pour les adultes que pour les enfants.
                            </p>

                            <p>L'entrée et la consultation sur place sont gratuites.</p>

                            <p>Selon vos goûts, au fil du temps, vous regarderez une exposition, vous assisterez à l'heure du conte avec votre (vos)                                enfant(s) ou participerez à un atelier créatif. Enfin vous écouterez un concert ou surferez sur Internet.</p>
                        </article>
                        
                        <article>
                            <h2> Horaires </h2>
                            <h4>Espace adultes et audiovisuel</h4>
                            <ul>
                                <li>Mardi : 13h30 - 18h00</li>
                                <li>Mercredi : 09h00 - 12h00 / 13h30 - 18h00</li>
                                <li>Jeudi et Vendredi : 13h30 / 18h00</li>
                                <li>Samedi : 09h00 - 12h30 / 13h30 - 18h00</li>
                            </ul>
                            <h4>Espaces jeunesses</h4>
                            <ul>
                                <li>Mardi : 16h00 / 18h00</li>
                                <li>Mercredi : 13h30 - 18h00</li>
                                <li>Samedi : 09h00 - 12h30 / 13h30 - 18h00</li>
                                <li>Ouverture de l'espace jeunesse tous les jours à 13h30 pendant les vacances</li>
                            </ul>
                        </article>
                    </div>
                </section>";
        return $res;
    }
    
    // Function affichage Form Search:
    private static function afficheFormSearch(){
        $res= " <article>
                    <h2> RECHERCHER </h2>
                    <form action='index.php?action=action_search' method='POST'>
                        <div>
                            <div>
                                <input name='keywords' type='text' placeholder='Mots-clés'>
                                <label><img src='./data/icons/search.png' alt='search'></label>
                            </div>
                            <div>
                                <select name='type'>
                                    <option value='Tous'> Tout type </option>
                                    <option value='Video'> Video </option>
                                    <option value='Audio'> Audio </option>
                                    <option value='Livre'> Livre </option>
                                </select>
                                <label><img src='./data/icons/select.png' alt='select'></label>
                            </div>
                            <div>
                                <select name='sort'>
                                    <option value='Tous'> Tout genre </option>
                                    <option value='Policier'> Policier </option>
                                    <option value='Action'> Action </option>
                                    <option value='Aventure'> Aventure </option>
                                    <option value='Classique'> Classique </option>
                                    <option value='Romance'> Romance </option>
                                    <option value='House'> House </option>
                                    <option value='Rock'> Rock </option>
                                </select>
                                <label><img src='./data/icons/select.png' alt='select'></label>
                            </div>
                        </div>
        		    <input type='hidden' value='action_search' name='action'>
                        <div>
                            <input type='reset' value=' '>
                            <input type='submit' value=' '>
                        </div>
                    </form>
                </article>";
        return $res;
    }
    
    // Function affichage Results Search (liste rÃ©sultat gÃ©nÃ©rÃ©e par fonction):
    private static function afficheResultSearch($tabResult){ //$tabResult = $doc
        $res= " <article>";
        foreach ($tabResult as $value){
                $res .= "<div>";
                $res .= "<div>";
        	$res .= "<h2><a href='index.php?action=action_ficheDetail&amp;referenceDoc=".$value->reference."'>".$value->title."</a></h2>";
        	$res .= "<h3>".$value->author."</h3>";
        	$res .= "<h4>".$value->type." - ".$value->getSort()."</h4>";
        	$res .= "<p>".$value->statutDoc()."</p>";
                $res .= "</div>";
                $res .= "<div>";
                $res .= "<img src='data/images/image_not_found.png' alt='Image indisponible' >";
        	$res .= "</div>";
                $res .= "</div>";
        }
        $res .= "</article>";
        
        return $res;
    }
    
    // Function affichage page Search:
    private static function afficheSearch($centerLeft,$centerRight){
        $res= " <section id='search'>
                    <div>
                        $centerLeft
                        <hr>
                        $centerRight
                    </div>
                </section>";
        return $res;
    }
	
    //Function affichage page detailDoc:
    private static function afficheDetailDoc($tabResult, $dateReturn){
        $res = " <section id='detail'>";
        $res.= " <div>";
        
        foreach ($tabResult as $value){
        $res .= "<article>";
        $res .=     "<div>";
        $res .=         "<img src='data/images/image_not_found.png' alt='Image indisponible' >";
        $res .=     "</div>";
        $res .=     "<div>";
        $res .=         "<h2>".$value->title."</h2>";
        $res .=         "<h3>".$value->author."</h3>";
        $res .=         "<h3>".$value->dateParution."</h3>";
        //$res .= "<h3>".$value->dateAdd."</h3>";
        $res .=         "<h4>".$value->type."</h4>";
        $res .=         "<h4>".$value->getSort()."</h4>";
        $res .=         "<p>".$value->statutDoc();
        // si le statut = emprunté alors on affiche la date de retour du document sinon rien
        if($value->statutDoc()=="disponible"){
            $res .= "</p>";
            $res .= "<label id='dispo'></label>";
        }else{
            if($value->statutDoc()=="emprunté"){
                $res .= " (retour : $dateReturn->dateReturn)</p>";
            }
            else{
                $res .= "</p>";
            }
            $res .= "<label id='indispo'></label>";
        }
        $res .=     "</div>";
        $res .= "</article>";
        
        $res .= "<article>";
        $res .=     "<p>".$value->description."</p>";        
        $res .= "</article>";
        }
        
        $res.= " </div>";
        $res.= " </section>";
        
	return $res;
    }
	
	//Function affiche Profil
	public function afficheProfil(){
		$res= " <section id='profil'>
                    <div>
			<article>
                            <h2> Profil de ". $_POST["adherent"]->login." </h2>
					";
		$res.= "    <p> N° Adhérent: ".$_POST["adherent"]->numAdherent." </p>
			    <p> Nombre d'emprunt(s): ".count($_POST["loaning"])." </p>
                        </article>
                        <hr>";
		$res.= "<table>
                            <tr>
				<th> TITRE </th>
                                <th> TYPE </th>
                                <th> AUTHOR </th>
                                <th> RETOUR AVANT </th>
                            </tr>";
							$i = 0;
                            foreach ($_POST["document"] as $value){
                                $res.="<tr>";
                                    $res.="<td>$value->title</td>";
                                    $res.="<td>$value->type</td>";
                                    $res.="<td>$value->author</td>";
                                    $t=Loaning::findEmpruntByReferenceDoc($value->reference);								
                                    $res.="<td>".$t->dateReturn."</td>";
                                    $res.="</tr>";
									$i++;
                            }
       $res.="		</table>
                        </div>
                    </section>";
		
         return $res;
	}
    
    // Function affichage GÃ©nÃ©ral:
    public function afficheGeneral($select){
        switch($select){
            //Affichage de la page index:
            case 'index':
                $this->center=$this->afficheAccueil();
            break;
            //Afficher la page search:
            case 'search':
                $this->centerLeft=$this->afficheFormSearch();
                $this->centerRight=$this->afficheResultSearch($this->tabResult);
                $this->center=$this->afficheSearch($this->centerLeft,$this->centerRight);
            break;
			//Afficher fiche détail d'un Document
			case 'detailDoc':
                $this->center=$this->afficheDetailDoc($this->tabResult,$this->dateReturn);
            break;
			//Afficher le profil d'un Adherent
            case 'profil':
                $this->center=$this->afficheProfil();
            break;
        }
        echo $this->afficheDebutHtml();
        echo $this->afficheHeader();
        echo $this->center;
        echo $this->afficheFooter();
        echo $this->afficheFinHtml();
    }
    
}
?>