<?php

class Controleur {

    // Function Controleur:
    public function __construct() {
        // rien Ã  faire
    }

    // Function Dispatch:
    public function dispatch($get){
        switch($get['action']){
            case 'action_index':
                $this->actionPageIndex();
            break;
            case 'action_search':
                $this->actionPageSearch();
            break;
			case 'action_ficheDetail':
                $this->actionFicheDetail();
            break;
            case 'authOk' :
				$this->authOk($get);
            break;
            case 'logout' :
				$this->logout($get);
            break;
			case 'action_profil' :
				$this->actionPageProfil($get);
            break;
        }
    }
    
    //  Action pour afficher page index:
    private function actionPageIndex(){
        $v=new View();
        $v->afficheGeneral('index');
    }
	
	//Action pour afficher le profil d'un adherent
	 private function actionPageProfil($get){
		$v=new View();
		$nomAdherent = $get["nomAdherent"];
		$adherent = Adherent::findByLogin($nomAdherent);
		$loaning = Loaning::findEmpruntByNumAdherent($adherent->numAdherent);
		 
		$document = array();
		foreach($loaning as $value){
			$document[] = Document::findByReference2($value->referenceDoc);
		}
		 
		 $_POST["adherent"] = $adherent;
		 $_POST["document"]= $document;
		 $_POST["loaning"]=  $loaning;
		 $v->afficheGeneral('profil');
	 }
	
	//Action pour afficher détaile d'un Document
	 private function actionFicheDetail(){
		$v=new View();
		//$d = new Document();
		
		//Récupe infos Document par Reference
		$reference = $_GET['referenceDoc'];
		$document = Document::findByReference($reference);
		//var_dump($document); 
         
        //verifie si l'id statut est a 2 soit si le document est emprunté afin d'afficher la date de retour 
        foreach($document as $value){
            if($value->idStatut == 2){ 
                $documentLoaning = Loaning::findEmpruntByReferenceDoc($reference);
                $v->dateReturn = $documentLoaning;
            }
        }
         
		$v->tabResult = $document; 
		$v->afficheGeneral('detailDoc');
	 }
    
    //  Action pour afficher page search:
    private function actionPageSearch(){
        $v=new View();
        $d = new Document();
        
        //Trier en fonction du type
        if (isset($_POST['type'])){
        	$type = $_POST['type'];
        	if ($type == 'Tous'){
        		$doctype = $d->findAll();
        	}else{
        		$doctype = $d->findByType($type);
        	}
        }else{
        	$doctype = $d->findAll();
        }
       
        //Trier en fonction du genre
        if (isset($_POST['sort'])){
        	$sort = $_POST['sort'];
        	if ($sort == 'Tous'){
        		$docSort = $d->findAll();
        	}else{
				$docSort = $d->findBySort($sort);
			}        
        }else{
        	$docSort = $d->findAll();
        }
        
        
        
        
        //Trier en fonction du mot cle
        if (isset($_POST['keywords'])){
        	$keywords = $_POST['keywords'];
        	if (empty($keywords)){
        		$docKey = $d->findAll();
        	}else{
        		$docKey = $d->findByKeywords($keywords);
        	}
        }else{
        	$docKey = $d->findAll();
        }
        
        $v->tabResult = array_intersect($doctype, $docSort, $docKey); //array_merge($doctype,$doc)
//         echo "<pre>";
//         print_r($doctype);
//         echo "</pre>";
        $v->afficheGeneral('search');
    }
    
    
        //Session
    private function authOk($get){
		$s = new Session();
		$s->login($get['login'], $get['password']);
		$v = new View();
		$v->afficheGeneral('index');
	}
    
    private function logout($get){
		$s = new Session();
		$s->logout();
        $v = new View();
		$v->afficheGeneral('index');
	}
    
}
?>   