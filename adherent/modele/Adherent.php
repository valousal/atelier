<?php
class Adherent{

	private $numAdherent; 
	private $name;
	private $firstname;
	private $mail;
	private $dateRegistration;
    private $birthday;
    private $numPhone;
    private $login;
    private $password = null;
    private $salt = null;
    private $idLibrary;
    
    
	public function __construct(){
	
	}
	
	
	
	public function __toString() {
			return "[". __CLASS__ . "] numAdherent : ". $this->numAdherent.":
				name ". $this->name.":
				firstname ". $this->firstname.":
				mail ". $this->mail.":
                dateRegistration ". $this->dateRegistration.":
                birthday ". $this->birthday.":
                numPhone ". $this->numPhone.":
                login ". $this->login.":
                password ". $this->password.":
                salt ". $this->salt.":
                idLibrary ". $this->idLibrary;
	}




	public function __get($attr_name) {
		if (property_exists( __CLASS__, $attr_name)) { 
		  return $this->$attr_name;
		} 
		$emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
		throw new Exception($emess, 45);
	}
	   

	public function __set($attr_name, $attr_val) {
		if (property_exists( __CLASS__, $attr_name)) {
		  $this->$attr_name=$attr_val; 
		  return $this->$attr_name;
		} 
		$emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
		throw new Exception($emess, 45);	
	}
    
    
     //********************************************************************************/
    //********************************fonction findAll*********************************/
    //*******************************************************************************/
    public static function findAll(){
        $db = Base::getConnection();
		$query = $db->prepare("SELECT * FROM adherent");
		Try{
			$query->execute();
			$arrayDocAll = Array();
			while ($d = $query->fetch(PDO::FETCH_OBJ)){ //fetchAll?
				$res = new Adherent;
				$res->numAdherent = $d->numAdherent;
				$res->name = $d->name;
				$res->firstname = $d->firstname;
				$res->mail = $d->mail;
				$res->dateRegistration = $d->dateRegistration;
				$res->birthday = $d->birthday;
				$res->numPhone = $d->numPhone;
				$res->login = $d->login;
				$res->password = $d->password;
                $res->salt = $d->salt;
                $res->idLibrary = $d->idLibrary;
				$arrayDocAll[] = $res;
			}
			return $arrayDocAll;
		}catch(PDOExeption $e){
			new PDOExeption($e->getMessage());
		}
    }
    
    
    //********************************************************************************/
    //********************************fonction findbyName*********************************/
    //*******************************************************************************/
    public static function findByLogin($login) {			
		$db = Base::getConnection();
		$query = $db->prepare("SELECT * FROM adherent WHERE login=:login");
		Try{
			$query->execute(array(":login"=>$login));
			$res = "";
			$d = $query->fetch(PDO::FETCH_OBJ); 
                if ($d != false){
				    $res = new Adherent;
                    $res->numAdherent = $d->numAdherent;
                    $res->name = $d->name;
                    $res->firstname = $d->firstname;
                    $res->mail = $d->mail;
                    $res->dateRegistration = $d->dateRegistration;
                    $res->birthday = $d->birthday;
                    $res->numPhone = $d->numPhone;
                    $res->login = $d->login;
                    $res->password = $d->password;
                    $res->salt = $d->salt;
                    $res->idLibrary = $d->idLibrary;
			     }else{
                    $res = null;
                }
                
                
			         return $res;
		}catch(PDOExeption $e){
			new PDOExeption($e->getMessage());
		}	
    }
}

?>
    
    