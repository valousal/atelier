<?php
class Loaning{

	private $numAdherent; 
	private $referenceDoc;
	private $dateLoaning;
	private $dateReturn;
	
	public function __construct(){
	
	}
	
	
	
	public function __toString() {
			return "[". __CLASS__ . "] numAdherent : ". $this->numAdherent.":
				referenceDoc ". $this->referenceDoc.":
				dateLoaning ". $this->dateLoaning.":
				dateReturn ". $this->dateReturn;
	}




	public function __get($attr_name) {
		if (property_exists( __CLASS__, $attr_name)) { 
		  return $this->$attr_name;
		} 
		$emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
		throw new Exception($emess, 45);
	}
	   

	public function __set($attr_name, $attr_val) {
		if (property_exists( __CLASS__, $attr_name)) {
		  $this->$attr_name=$attr_val; 
		  return $this->$attr_name;
		} 
		$emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
		throw new Exception($emess, 45);	
	}
    
    
    public static function findEmpruntByNumAdherent($numAdherent){
    	$db = Base::getConnection();
    	$query = $db->prepare("SELECT * FROM loaning WHERE numAdherent=:numAdherent");
    	Try{
    		$query->execute(array(":numAdherent"=>$numAdherent));
    		$arrayLoaningAdherent = Array();
    		while ($d = $query->fetch(PDO::FETCH_OBJ)){ //fetchAll?
    			$res = new Loaning;
    			$res->numAdherent = $d->numAdherent;
    			$res->referenceDoc = $d->referenceDoc;
    			$res->dateLoaning = $d->dateLoaning;
    			$res->dateReturn = $d->dateReturn;
    	
    			$arrayLoaningAdherent[] = $res;
    		}
    		return $arrayLoaningAdherent;
    	}catch(PDOExeption $e){
    		new PDOExeption($e->getMessage());
    	}
    }
    
    
        public static function findEmpruntByReferenceDoc($referenceDoc){
    	$db = Base::getConnection();
    	$query = $db->prepare("SELECT * FROM loaning WHERE referenceDoc=:referenceDoc");
    	Try{
    		$query->execute(array(":referenceDoc"=>$referenceDoc));
    		$arrayLoaningAdherent = Array();
    		$d = $query->fetch(PDO::FETCH_OBJ);
            
            $res = new Loaning;
            $res->numAdherent = $d->numAdherent;
            $res->referenceDoc = $d->referenceDoc;
            $res->dateLoaning = $d->dateLoaning;
            $res->dateReturn = $d->dateReturn;
    		
            return $res;
    	}catch(PDOExeption $e){
    		new PDOExeption($e->getMessage());
    	}
    }
    
    
}

?>