<?php

class Session{
	private $user_id;
	private $logedin=false;
	
	public function __construct() {
		$this->check_login();
	}
	
	public function __get($attr_name) {
		if (property_exists( __CLASS__, $attr_name)) { 
		  return $this->$attr_name;
		} 
		$emess = __CLASS__ . ": unknown member $attr_name (getAttr)";
		throw new Exception($emess, 45);
	}
   
    public function __set($attr_name, $attr_val) {
		if (property_exists( __CLASS__, $attr_name)) {
		  $this->$attr_name=$attr_val; 
		  return $this->$attr_name;
		} 
		$emess = __CLASS__ . ": unknown member $attr_name (setAttr)";
		throw new Exception($emess, 45);		
	 }
	
	public function is_logged_in() {
		return $this->logedin;
	}
	
	private function check_login(){
		if(isset($_SESSION['user_id'])) {
			$this->user_id = $_SESSION['user_id'];
			$this->logedin = true;
		}else{
			unset($this->user_id);
			$this->logedin = false;
		}
	}
	
	public function login($user, $pass){
		$u = Adherent::findByLogin($user);
        if ($u!=null){
            $h = crypt($pass, $u->salt);
            if($h == $u->password){
                $this->user_id = $user;
                $_SESSION['user_id'] = $user;
                $this->logedin = true;
            }
        }
	}
	
	public function logout(){
		unset ($_SESSION['user_id']); //unset
		unset ($this->user_id);
		$this->logedin=false;
		session_destroy();
	}

}

?>
