<?php 

function __autoload($class_name){
  require_once($class_name.".php");
}


function test(){
  echo "
<!DOCTYPE html>
<html lang=\"fr\">
    <head>
        <meta  charset=\"utf-8\">
        <title>Page tests du wiki</title>
        <link rel=\"stylesheet\" href=\"style.css\"> 
    </head>
<body>\n";

  echo "
<article>
<h1>Test de connexion à la base </h1>\n";

  $db = Base::getConnection();

  echo "<p>Si il n'y a pas d'exception soulevée la connexion a réussie.</p>
</article>\n";
  
  /* recupère quelques informations du SGBD */

  $attributes = array( "AUTOCOMMIT", "ERRMODE", "CASE", "CLIENT_VERSION", "CONNECTION_STATUS", "ORACLE_NULLS", "PERSISTENT", "SERVER_INFO", "SERVER_VERSION" );

  echo "<article>
<h1>Quelques informations sur le SGBD </h1>
<ul>\n";
  
  foreach ($attributes as $val) {
    echo "<li> PDO::ATTR_$val : ".
      $db->getAttribute(constant("PDO::ATTR_$val")) . "</li>\n";
  }
  echo"</ul>
</article>";

  echo"
</body>
</html>";

}


test();