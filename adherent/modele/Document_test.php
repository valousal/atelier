<?php

function __autoload($class_name){
  require_once($class_name.".php");
}

function test(){
  echo "
<!DOCTYPE html>
<html lang=\"fr\">
    <head>
        <meta  charset=\"utf-8\">
        <title>Page tests du wiki</title>
        <link rel=\"stylesheet\" href=\"style.css\"> 
    </head>
<body>\n";

	echo "<h1>Test FindBySort</h1>";
	$docTest = Document::findBySort('House');
	foreach ($docTest as $value) { 
      echo $value->reference;
	  echo $value->title;
	  echo "<br>";
    }

    echo "<h1>Test FindAll</h1>";
    $docTestAll = Document::findAll();
    foreach ($docTestAll as $value) {
	    echo $value->reference;
	    echo $value->title;
	    echo "<br>";
    }
    
    echo "<h1>Test FindByReference</h1>";
    $docTestRef = Document::findByReference(3);
    foreach ($docTestRef as $value) {
	    echo $value->reference;
	    echo $value->title;
	    echo "<br>";
    }
    
    echo "<h1>Test FindByKeywords</h1>";
    $docTestBookAll = Document::findByKeywords('album');
    foreach ($docTestBookAll as $value) {
    	echo $value->reference;
    	echo $value->title;
    	echo "<br>";
    }
    
    echo "<h1>Test FindByType</h1>";
    $docTestType = Document::findByType('Audio');
    foreach ($docTestType as $value) {
    echo $value->reference;
    echo $value->title;
    echo "<br>";
    }
	
	echo "<h1>Test Find Statut Document</h1>";
    $ddd = new Document();
	$ddd->reference= 999;
	$ddd->title= "documentTest";
	$ddd->dateParution = 2014-12-25;
	$ddd->author = "Michel";
	$ddd->description= "description du document test";
	$ddd->dateAdd= "8989898";
	$ddd->idLibrary= 1;
	$ddd->idStatut= 2;
	$ddd->type= "Video";
	echo $ddd->statutDoc();
	
	 $ddd2 = new Document();
	$ddd2->reference= 3;
	echo $ddd2->getSort();
    
    echo"
</body>
</html>";

}


test();